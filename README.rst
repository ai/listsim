
lists.im - newsletter management
================================


How to run this software locally
--------------------------------

Once you've checked out the git repository, set up a test environment
with the following commands::

    $ virtualenv ./venv
    $ ./venv/bin/python setup.py develop

Then create a configuration file named, for instance, 'test.conf, with
the following contents::

    BASE_URL = 'http://localhost:5000'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///data.db'
    SECRET_KEY = 'secret'
    DEBUG = True

Create the database and populate it with test data::

    $ export APP_CONFIG=$PWD/test.conf
    $ ./venv/bin/python listsim/manage.py initdb
    $ ./venv/bin/python listsim/manage.py create_test_data my.domain
    $ ./venv/bin/python listsim/manage.py runserver

You should now have a lists.im instance running locally on port
5000. Login with one of the newly created test users (for example,
"user1" with password "11") and try out the functionality. No emails
will be sent by the test instance.



Maintaining translations
------------------------

The translations are managed by the Flask-Babel extension, and there
are some manual steps involved in maintaining them.


Updating the source code or templates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have modified the original English messages in the source code
or the templates, you need to regenerate the top-level `messages.pot`
file and run a merge step::

    $ ./venv/bin/pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot .
    $ ./venv/bin/pybabel update -i messages.pot -d listsim/translations


Adding a new language to the application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you're adding a new translation for an unsupported language, you
need to initialize the language-specific `messages.po` file::

    $ ./venv/bin/pybabel init -i messages.pot -d listsim/translations -l $LANG

Then, you need to modify the `AVAILABLE_LOCALES` setting in the
`listsim/__init__.py` file so that users can select the new
translation.


Updating a translation
~~~~~~~~~~~~~~~~~~~~~~

To update an existing translation, edit the `messages.po` file for
that language in the `listsim/translations/$LANG/LC_MESSAGES`
directory.  After you've done this, compile the updated translation::

    $ ./venv/bin/pybabel compile -d listsim/translations



License
-------

Copyright 2013 Autistici/Inventati <info@autistici.org>.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

