import functools
import logging
import Queue
import threading
from datetime import datetime

from listsim import app
from listsim import mailer
from listsim import utils
from listsim.model import *

log = logging.getLogger(__name__)


def with_session(fn):
    @functools.wraps(fn)
    def _with_session(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        finally:
            try:
                db.session.rollback()
            except:
                pass
            db.session.remove()
    return _with_session


def process_delivery(delivery_id):
    delivery = Delivery.query.get(delivery_id)
    current_pos = delivery.checkpoint
    addrs = delivery.payload['addrs']
    num_addrs = len(addrs)
    addrs = addrs[current_pos:]

    m = mailer.Mailer()
    msggen = mailer.MessageGenerator(
        delivery.message.newsletter,
        delivery.message.subject,
        delivery.message.text,
        delivery.message.html
        )

    # Read Member objects efficiently (in batches), generate
    # personalized messages and deliver them. We will bail out on the
    # first serious (i.e., non recipient-specific) SMTP error.
    for chunk in utils.group_by_chunks(addrs, 500):
        # Build addr->map dict since we can't guarantee that the
        # results of Member.from_emails will respect ordering (so we
        # iterate on 'chunk' instead).
        addr2mem = dict((x.email, x) for x in Member.from_emails(chunk))
        for addr in chunk:
            msg = msggen.personalize(addr2mem[addr])
            # Catch non-fatal errors and proceed.
            try:
                m.send(msg, addr)
            except SendError, e:
                log.error('error sending message %d to %s: %s',
                          delivery.id, addr, str(e))
                delivery.num_errors += 1

            # Increase the checkpoint. We have to commit the session
            # to ensure that the checkpoint is persisted. This is, of
            # course, quite slow.
            current_pos += 1
            progress = int(100 * current_pos / num_addrs)
            delivery.update(current_pos, progress=progress)
            db.session.add(delivery)
            db.session.commit()

    # Mark the delivery as done.
    delivery.update(current_pos, progress=100, done=True)
    db.session.add(delivery)
    db.session.commit()


@with_session
def process_delivery_in_session(delivery_id):
    process_delivery(delivery_id)


class ProcessDeliveryThread(threading.Thread):

    def __init__(self, input_queue):
        threading.Thread.__init__(self)
        self.input_queue = input_queue

    @with_session
    def set_delivery_state(self, delivery_id, state):
        d = Delivery.query.get(delivery_id)
        d.state = state
        db.session.add(d)
        db.session.commit()

    def run(self):
        with app.app_context():
            while True:
                delivery_id = self.input_queue.get()
                if delivery_id is None:
                    break
                log.info('starting delivery %d', delivery_id)
                self.set_delivery_state(delivery_id, 'running')
                process_delivery_in_session(delivery_id)
                self.set_delivery_state(delivery_id, 'done')


@with_session
def recover_deliveries(queue):
    """Scan the delivery table at startup for deliveries that were running."""
    for delivery in Delivery.query.filter_by(state='running'):
        log.info('recovering delivery %d', delivery.id)
        queue.put(delivery.id)


@with_session
def schedule_deliveries(queue):
    """Scan the delivery table and queue deliveries that are bound to run now."""
    now = datetime.utcnow()
    for delivery in Delivery.query.filter(
        (Delivery.state == 'pending') & (Delivery.start_at < now)):
        log.info('scheduling delivery %d', delivery.id)
        queue.put(delivery.id)


class SchedulerThread(threading.Thread):

    INTERVAL = 300

    def __init__(self, n_workers=5):
        threading.Thread.__init__(self)
        self.queue = Queue.Queue(n_workers)
        self.stop_event = threading.Event()
        self.workers = [
            ProcessDeliveryThread(self.queue)
            for x in xrange(n_workers)]
        for x in self.workers:
            x.start()

    def stop(self):
        log.info('stopping mailer...')
        self.stop_event.set()
        for x in self.workers:
            self.queue.put(None)
        for x in self.workers:
            x.join()
        return self.join()

    def run(self):
        with app.app_context():
            recover_deliveries(self.queue)
            while not self.stop_event.isSet():
                schedule_deliveries(self.queue)
                self.stop_event.wait(self.INTERVAL)

