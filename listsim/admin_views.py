import functools
import re
from datetime import datetime
from flask import render_template, redirect, abort, request, flash, url_for
from flask.ext.babel import gettext, lazy_gettext
from flask.ext.login import current_user, login_required
from flask.ext.wtf import Form
from wtforms import Field, TextAreaField, TextField, DateTimeField, \
    BooleanField, SelectField, IntegerField, HiddenField
from wtforms.validators import AnyOf, Required
from listsim import app
from listsim.auth import check_newsletter_owner
from listsim.model import *


def with_newsletter_owner(fn):
    @functools.wraps(fn)
    def _with_newsletter_owner(nl_id, *args, **kwargs):
        nl = Newsletter.query.get_or_404(nl_id)
        check_newsletter_owner(nl)
        return fn(nl, *args, **kwargs)
    return _with_newsletter_owner


class EditNewsletterForm(Form):
    public = TextField('public', validators=[AnyOf(['y', 'n'])])


@app.route('/admin/<nl_id>', methods=['GET', 'POST'])
@login_required
@with_newsletter_owner
def edit_newsletter(nl):
    form = EditNewsletterForm(public=(nl.public and 'y' or 'n'))
    if form.validate_on_submit():
        nl.public = (form.public.data == 'y')
        db.session.add(nl)
        db.session.commit()
        flash(gettext(u'Subscription policy updated.'), 'success')
    return render_template('edit_newsletter.html', newsletter=nl, form=form)


class EditNewsletterMetaForm(Form):
    description = TextAreaField(
        lazy_gettext(u'Description (for the public subscription page)'),
        validators=[Required()])


@app.route('/admin/<nl_id>/meta', methods=['GET', 'POST'])
@login_required
@with_newsletter_owner
def edit_newsletter_meta(nl):
    form = EditNewsletterMetaForm(description=nl.description)
    if form.validate_on_submit():
        nl.description = form.description.data
        db.session.add(nl)
        db.session.commit()
        flash(gettext(u'Description updated.'), 'success')
        return redirect(url_for('edit_newsletter', nl_id=nl.id))
    return render_template('edit_newsletter_meta.html', newsletter=nl, form=form)


class NewMessageForm(Form):
    msg_id = HiddenField()
    send_test = HiddenField()
    group_id = HiddenField()
    subject = TextField(
        lazy_gettext(u'Subject (the newsletter name is always prepended)'),
        validators=[Required()])
    body = TextAreaField(
        lazy_gettext(u'Message body'), validators=[Required()])
    send_at = DateTimeField(lazy_gettext(u'Send time'))


@app.route('/admin/<nl_id>/message', methods=['GET', 'POST'])
@with_newsletter_owner
@login_required
def edit_newsletter_message(nl):
    form = NewMessageForm(send_at=datetime.utcnow(), send_test='y', group_id=-1)
    allow_send = False

    if form.validate_on_submit():

        # Autogenerate the text part of the message.
        text = utils.wrap_text(utils.html_to_text(form.body.data))

        if form.msg_id.data:
            # Verify that the message belongs to this newsletter.
            msg = Message.query.get_or_404(int(form.msg_id.data))
            if msg.newsletter != nl:
                abort(401)
            msg.subject = form.subject.data
            msg.html = form.body.data
            msg.text = text
        else:
            # Create a new Message object.
            msg = Message(newsletter=nl,
                          subject=form.subject.data,
                          html=form.body.data,
                          text=text)

        if form.send_test.data == 'y':
            recipient = current_user.email
            nl.send_message(msg, recipients=[recipient])
            flash(gettext(u'Test message sent to %s' % recipient), 'success')
        else:
            group = None
            group_id = int(form.group_id.data)
            if group_id > 0:
                group = Group.query.get_or_404(group_id)
                if group.newsletter != nl:
                    abort(400)
            send_at = form.send_at.data or datetime.now()
            nl.send_message(msg, group=group, start_at=send_at)
            flash(gettext(u'Message queued for delivery.'), 'success')

        db.session.add(msg)
        db.session.commit()

        # Redirect on send, stay on the same page for test.
        if form.send_test.data == 'n':
            return redirect(url_for('edit_newsletter', nl_id=nl.id))

        form.msg_id.data = msg.id
        allow_send = True

    form.send_test.data = 'y'
    return render_template('edit_newsletter_message.html', newsletter=nl, form=form,
                           allow_send=allow_send)


@app.route('/admin/<nl_id>/message/<msg_id>')
@with_newsletter_owner
@login_required
def show_message(nl, msg_id):
    msg = Message.query.get_or_404(msg_id)
    return render_template('show_message.html',
                           newsletter=nl, message=msg)


class MultipleIdField(Field):

    def process_formdata(self, values):
        self.data = [int(x) for x in values]


class SubscriptionsForm(Form):
    sub_id = MultipleIdField()
    action = TextField(validators=[AnyOf(['unsubscribe', 'set_group',
                                          'clear_group', 'create_group'])])
    arg = TextField()


class SubscriptionsFilterForm(Form):
    p = IntegerField()
    cf = BooleanField(lazy_gettext(u'Show only confirmed subscriptions'))
    g = SelectField(lazy_gettext(u'Filter group'), coerce=int)
    mq = TextField(lazy_gettext(u'Search'))


@app.route('/admin/<nl_id>/subscriptions', methods=['GET', 'POST'])
@with_newsletter_owner
@login_required
def edit_newsletter_subscriptions(nl):
    form = SubscriptionsForm()

    # If the action form has been submitted, run the specified command.
    if form.validate_on_submit():
        action = form.action.data
        arg = form.arg.data
        subs = [Subscription.query.filter_by(
                newsletter_id=nl.id, member_id=x).one()
                for x in form.sub_id.data]

        if action == 'unsubscribe' and subs:
            nl.unsubscribe([x.member.email for x in subs])
            db.session.add(nl)
            flash(gettext(u'Unsubscribed %d members.' % len(subs)), 'success')

        elif action == 'create_group':
            g = Group(newsletter=nl, name=arg,
                      subscriptions=subs)
            db.session.add(g)
            flash(gettext(u'Created new group "%s".' % arg), 'success')

        elif action == 'set_group' and subs:
            g = Group.query.get(int(arg))
            for s in subs:
                s.group = g
                db.session.add(s)

        elif action == 'clear_group':
            for s in subs:
                s.group = None
                db.session.add(s)

        db.session.commit()

    # Initialize the filtering form. We don't use validate_on_submit() here
    # because we don't want to be limited to POST requests.
    filter_form = SubscriptionsFilterForm(request.args, p=1, cf=True, g=-1)
    filter_form.g.choices = [(-1, 'All')] + [(g.id, g.name) for g in nl.groups]
    filter_form.validate()

    # Assemble the ORM query. Note that the Member objects are eagerly loaded
    # so that we only perform a single SQL query to render the subscriptions
    # table.
    query = (Subscription.state == STATE_SUBSCRIBED)
    if not filter_form.cf.data:
        query = query | (Subscription.state == STATE_OPT_IN)
    if filter_form.g.data > 0:
        query = query & (Subscription.group_id == filter_form.g.data)
    if filter_form.mq.data:
        query = query & Subscription.member.has(
            Member.email.like('%%%s%%' % filter_form.mq.data))
    subs = nl.subscriptions.filter(query).options(db.joinedload('member'))
    subs = subs.paginate(filter_form.p.data, per_page=20)

    # This callback function allows us to keep all the filtering arguments
    # when generating links for this page outside of filter_form. Used for
    # actions and pagination.
    def _uf(**args):
        endpoint_args = {
            'nl_id': nl.id,
            'p': filter_form.p.data,
            'g': filter_form.g.data,
            'mq': filter_form.mq.data}
        # Boolean fields are special.
        if filter_form.cf.data:
            endpoint_args['cf'] = 'y'
        endpoint_args.update(args)
        return url_for('edit_newsletter_subscriptions', **endpoint_args)

    return render_template('edit_newsletter_subscriptions.html',
                           newsletter=nl, subscriptions=subs,
                           form=form, filter_form=filter_form,
                           url_with_filter=_uf)


class AddressListField(TextAreaField):

    # A really minimal email-validation regexp.
    _email_pattern = re.compile(r'^\S+@[-.a-zA-Z0-9]+$')

    def process_formdata(self, values):
        items = values[0].split()
#        print 'ADDRESSLIST:', items
        self.data = [x for x in items
                     if x and self._email_pattern.match(x)]


class MassSubscribeForm(Form):
    addrs = AddressListField('addrs')


@app.route('/admin/<nl_id>/mass_subscribe', methods=['GET', 'POST'])
@with_newsletter_owner
@login_required
def mass_subscribe(nl):
    form = MassSubscribeForm()
    if form.validate_on_submit():
        n_sub = nl.subscribe(form.addrs.data)
        db.session.add(nl)
        db.session.commit()
        flash(gettext(u'Subscribed %d addresses.'  % n_sub), 'success')
        return redirect(url_for('edit_newsletter_subscriptions', nl_id=nl.id))
    return render_template('mass_subscribe.html', newsletter=nl, form=form)


@app.route('/admin/<nl_id>/mass_unsubscribe', methods=['GET', 'POST'])
@with_newsletter_owner
@login_required
def mass_unsubscribe(nl):
    form = MassSubscribeForm()
    if form.validate_on_submit():
        n_unsub = nl.unsubscribe(form.addrs.data)
        db.session.add(nl)
        db.session.commit()
        flash(gettext(u'Unsubscribed %d addresses.' % n_unsub) , 'success')
        return redirect(url_for('edit_newsletter_subscriptions', nl_id=nl.id))
    return render_template('mass_unsubscribe.html', newsletter=nl, form=form)


@app.route('/admin/<nl_id>/groups')
@with_newsletter_owner
@login_required
def manage_groups(nl):
    if request.args.get('action') == 'delete':
        g = Group.query.get(int(request.args['g']))
        if g not in nl.groups:
            abort(400)
        db.session.delete(g)
        db.session.commit()
        flash(gettext(u'Group deleted.'), 'success')
    return render_template('manage_groups.html', newsletter=nl)
