from flask import request
from flask.ext.login import current_user
from listsim import app, babel


@babel.localeselector
def get_locale():
    if current_user.is_authenticated():
        return current_user.preferred_lang
    available_languages = app.config['AVAILABLE_LOCALES'].keys()
    return request.accept_languages.best_match(available_languages)


