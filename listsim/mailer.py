import email
import email.header
import email.utils
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import logging
import smtplib
from flask import render_template

from listsim import app


log = logging.getLogger(__name__)


def format_addresses(addresses, header_name=None, charset=None):
    """This is an extension of email.utils.formataddr.
       Function expect a list of addresses [ ('name', 'name@domain'), ...].
       The len(header_name) is used to limit first line length.
       The function mix the use Header(), formataddr() and check for 'us-ascii'
       string to have valid and friendly 'address' header.
       If one 'name' is not unicode string, then it must encoded using 'charset', 
       Header will use 'charset' to decode it.
       Unicode string will be encoded following the "Header" rules : (
       try first using ascii, then 'charset', then 'uft8')
       'name@address' is supposed to be pure us-ascii, it can be unicode 
       string or not (but cannot contains non us-ascii) 
       
       In short Header() ignore syntax rules about 'address' field, 
       and formataddr() ignore encoding of non us-ascci chars.
    """
    header = email.header.Header(charset=charset, header_name=header_name)
    for i, (name, addr) in enumerate(addresses):
        if i != 0:
            # add separator between addresses
            header.append(',', charset='us-ascii') 
        # check if address name is a unicode or byte string in "pure" us-ascii 
        try:
            if isinstance(name, unicode):
                # convert name in byte string
                name = name.encode('us-ascii')
            else:
                # check id byte string contains only us-ascii chars
                name.decode('us-ascii')
        except UnicodeError:
            # Header will use "RFC2047" to encode the address name
            # if name is byte string, charset will be used to decode it first
            header.append(name)
            # here us-ascii must be used and not default 'charset'  
            header.append('<%s>' % (addr,), charset='us-ascii') 
        else:
            # name is a us-ascii byte string, i can use formataddr
            formatted_addr = email.utils.formataddr((name, addr))
            # us-ascii must be used and not default 'charset'  
            header.append(formatted_addr, charset='us-ascii') 
            
    return header


def angle_quote(s):
    return '<%s>' % s


class SendError(Exception):
    pass


class Mailer(object):
    """Deliver messages, a bit at a time.

    Will attempt to deliver multiple messages with a single SMTP
    connection. Errors are raised as soon as possible.
    """

    CONN_RECYCLE = 100

    def __init__(self):
        self.sent_emails = 0
        self.server = app.config.get('SMTP_SERVER', 'localhost')
        self.sender = app.config['SENDER_ADDR']
        self.conn = None

    def close(self):
        if self.conn:
            try:
                self.conn.quit()
            except:
                pass
        self.conn = None

    def connect(self):
        if self.conn is None:
            self.conn = smtplib.SMTP(self.server)

    def send(self, message, recipient):
        """Raises SendError if the message couldn't be delivered."""
        self.connect()
        try:
            result = self.conn.sendmail(app.config['BOUNCE_ADDR'], [recipient], message)
            if result.get(recipient):
                raise SendError(result[recipient])
        except smtplib.SMTPRecipientsRefused:
            self.close()
            raise SendError('Recipient refused')
        self.sent_emails += 1
        if self.sent_emails % self.CONN_RECYCLE == 0:
            self.close()


def add_nl_headers(msg, sender, nl):
    """Add common headers to outgoing messages."""
    msg['From'] = format_addresses([
            (sender, app.config['BOUNCE_ADDR'])])
    #msg['Return-Path'] = angle_quote(app.config['BOUNCE_ADDR'])
    msg['Priority'] = 'bulk'
    msg['Errors-To'] = app.config['BOUNCE_ADDR']
    if nl:
        msg['List-Id'] = angle_quote('%s.%s' % (
                nl.slug, app.config['LIST_DOMAIN']))
        msg['List-Help'] = angle_quote(nl.get_public_link())    


class MessageGenerator(object):
    """Create a MIME message for multiple recipients.

    Build most of the MIME message, then fill out user-specific
    details and serialize for each recipient.
    """

    def __init__(self, nl, message_text, message_html, subject):
        self.nl = nl

        # Assemble the message MIME parts.
        msg = text_part = html_part = None
        if message_text:
            msg = text_part = MIMEText(message_text, 'plain')
        if message_html:
            msg = html_part = MIMEText(message_html, 'html')
        if message_text and message_html:
            msg = MIMEMultipart('alternative', None, [text_part, html_part])

        # Set the common headers.
        msg['Subject'] = email.header.Header(subject)
        add_nl_headers(msg, nl.name, nl)

        self.msg = msg

    def personalize(self, member):
        self.msg['To'] = member.email
        self.msg['List-Unsubscribe'] = angle_quote(
            member.get_unsubscribe_link(self.nl))
        return self.msg.as_string()


class SystemMessageGenerator(object):
    """Generator of internal messages."""

    def __init__(self):
        self.sender = "%s Newsletter Service" % app.config['LIST_DOMAIN']

    def personalize(self, template_name, member, nl):
        msg = email.message_from_string(
            render_template('email/%s.txt' % template_name,
                            member=member, newsletter=nl))
        msg['To'] = member.email
        add_nl_headers(msg, self.sender, nl)
        return msg.as_string()

