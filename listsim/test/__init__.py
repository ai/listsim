import logging
from flask.ext.testing import TestCase
from fixture import SQLAlchemyFixture
from fixture.style import NamedDataStyle
from listsim import create_app
from listsim import mailer
from listsim import model
from listsim.model import db
from listsim.test import datasets


class ModelTestBase(TestCase):

    CONFIG = dict(
        BASE_URL='/',
        SECRET_KEY='test secret',
        SQLALCHEMY_DATABASE_URI='sqlite://',
        SQLALCHEMY_RECORD_QUERIES=False,
        #SQLALCHEMY_ECHO=True,
        TESTING=True,
        WTF_CSRF_ENABLED=False,
        SENDER_ADDR='sender@localhost',
        BOUNCE_ADDR='bounce@localhost',
        LIST_DOMAIN='example.com',
        )

    def create_app(self):
        return create_app(self.CONFIG)

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class ModelTestBaseWithFixture(ModelTestBase):

    DATASETS = (datasets.NewsletterData,
                datasets.MemberData,
                datasets.SubscriptionData,
                datasets.GroupData)

    def setUp(self):
        ModelTestBase.setUp(self)
        self.dbfixture = SQLAlchemyFixture(
            env=model,
            session=db.session(),
            style=NamedDataStyle())
        self.data = self.dbfixture.data(*self.DATASETS)
        print 'Inserting initial data...'
        self.data.setup()
        print 'Fixtures loaded.'

    def tearDown(self):
        try:
            print 'Cleaning fixtures...'
            self.data.teardown()
            print 'Fixtures removed.'
        finally:
            ModelTestBase.tearDown(self)


class AppTestBase(ModelTestBaseWithFixture):

    DATASETS = datasets.all_datasets

    def login(self, user=None):
        if user is None:
            user = self.data.UserData.test_user
        print 'logging in as', user.username
        resp = self.client.get('/login')
        self.assert200(resp)
        return self.client.post('/login', data=dict(
                username=user.username, password=user.password),
                                follow_redirects=True)

    def logout(self):
        return self.client.get('/logout', follow_redirects=True)


def _new_fake_mailer(sent_emails):
    class _FakeMailer(object):
        """Stub mailer.Mailer object that records sent emails."""

        def send(self, msg, addr):
            sent_emails.setdefault(addr, []).append(msg)

        def close(self):
            pass

    return _FakeMailer


class MailTestMixin(object):

    def setUp(self):
        self.sent_emails = {}
        self.old_mailer_class = mailer.Mailer
        mailer.Mailer = _new_fake_mailer(self.sent_emails)

    def tearDown(self):
        mailer.Mailer = self.old_mailer_class
