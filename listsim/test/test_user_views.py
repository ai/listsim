from listsim.model import *
from listsim.test import *


class UserViewsTest(AppTestBase, MailTestMixin):

    def setUp(self):
        AppTestBase.setUp(self)
        MailTestMixin.setUp(self)

    def tearDown(self):
        MailTestMixin.tearDown(self)        
        AppTestBase.tearDown(self)

    def test_splash_public(self):
        response = self.client.get('/')
        self.assert200(response)

    def test_splash_redirect_auth(self):
        self.login()
        response = self.client.get('/')
        self.assert_redirects(response, '/nls')

    def test_list_newsletters_public(self):
        response = self.client.get('/nls')
        self.assert200(response)
        self.assertTrue(
            self.data.NewsletterData.public.name in response.data)
        self.assertFalse(
            self.data.NewsletterData.nl1.name in response.data)

    def test_list_newsletters_auth_contains_own_nls(self):
        self.login()
        response = self.client.get('/nls')
        self.assert200(response)
        self.assertTrue(
            self.data.NewsletterData.public.name in response.data)
        self.assertTrue(
            self.data.NewsletterData.nl1.name in response.data)

    def test_show_newsletter_404(self):
        response = self.client.get('/n/nonexisting')
        self.assert404(response)

    def test_show_newsletter_public(self):
        nl = self.data.NewsletterData.public
        response = self.client.get('/n/%s' % nl.slug)
        self.assert200(response)
        self.assertTrue(nl.name in response.data)

    def test_show_newsletter_public_subscribe(self):
        nl = self.data.NewsletterData.public
        response = self.client.post('/n/%s' % nl.slug, data={'email': 't@t.org'})
        self.assert200(response)

        # Check the db
        m = Member.get_by_email('t@t.org')
        self.assertTrue(m is not None)

        # Check that an email was sent to the right user
        self.assertEquals(1, len(self.sent_emails))
        self.assertEquals('t@t.org', self.sent_emails.keys()[0])

    def test_unsubscribe(self):
        # Create our own unsubscription link.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        nl = Newsletter.get_by_name(self.data.NewsletterData.nl1.name).one()
        response = self.client.get(
            m.get_unsubscribe_link(nl))
        self.assert200(response)

        # Reload object from the db and verify.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assertEquals(STATE_OPT_OUT,
                          m.get_subscription(nl).state)

        self.assertEquals({}, self.sent_emails)

    def test_unsubscribe_fails_with_bad_token(self):
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assert400(
            self.client.get(
                '/unsubscribe/%s/abracadabra' % m.email))

    def test_unsubscribe_fails_with_bad_email(self):
        self.assert404(
            self.client.get(
                '/unsubscribe/nonexisting@example.com/abracadabra'))

    def test_confirmation(self):
        # Create our own unsubscription link.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        nl = Newsletter.get_by_name(self.data.NewsletterData.nl1.name).one()
        response = self.client.get(
            m.get_confirmation_link(nl))
        self.assert200(response)

        # Reload object from the db and verify.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assertEquals(STATE_SUBSCRIBED,
                          m.get_subscription(nl).state)

        self.assertEquals({}, self.sent_emails)

    def test_confirmation_fails_with_bad_link(self):
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assert400(
            self.client.get(
                '/confirm/%s/abracadabra' % m.email))

    def test_confirmation_fails_with_bad_email(self):
        self.assert404(
            self.client.get(
                '/confirm/nonexisting@example.com/abracadabra'))

    def test_global_opt_out(self):
        # Create our own global_opt_out link.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        response = self.client.get(
            m.get_global_opt_out_link())
        self.assert200(response)

        # Reload object from the db and verify.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assertEquals(True, m.global_opt_out)

    def test_global_opt_out_fails_with_bad_link(self):
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assert400(
            self.client.get(
                '/opt_out/%s/abracadabra' % m.email))

    def test_global_opt_out_fails_with_bad_email(self):
        self.assert404(
            self.client.get(
                '/opt_out/nonexisting@example.com/abracadabra'))
