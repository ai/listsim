from listsim.model import *
from listsim.test import *


class AdminViewsTest(AppTestBase):

    def setUp(self):
        AppTestBase.setUp(self)
        self.nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()

    def test_edit_newsletter_noauth(self):
        # Test that admin views are login-protected.
        #self.assertRedirects(
        #    self.client.get('/admin/%d' % self.nl.id),
        #    '/login?next=%%2Fadmin%%2F%d' % self.nl.id)
        self.assert401(
            self.client.get('/admin/%d' % self.nl.id))

    def test_edit_newsletter_bad_user(self):
        # Test with the wrong_newsletter_owner.
        self.login(self.data.UserData.user2)
        self.assert401(
            self.client.get('/admin/%d' % self.nl.id))

    def test_edit_newsletter(self):
        self.login()
        response = self.client.get('/admin/%d' % self.nl.id)
        self.assert200(response)
        self.assertTrue(self.nl.name in response.data)

    def test_edit_newsletter_meta(self):
        self.login()
        self.assert200(
            self.client.get('/admin/%d/meta' % self.nl.id))
        self.assertRedirects(
            self.client.post('/admin/%d/meta' % self.nl.id,
                             data={'description': u'New description'}),
            '/admin/%d' % self.nl.id)

        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertEquals(u'New description', nl.description)
