import itsdangerous
from listsim.model import *
from listsim.test import *

TEST_EMAIL = 'user@example.org'


class ModelSmokeTest(ModelTestBase):

    def test_new_nl(self):
        # Check that we can create a Newsletter object.
        nl = Newsletter(name=u'uhm uhm')
        db.session.add(nl)
        db.session.commit()

    def test_new_member(self):
        # Check that we can create a Member object.
        test_email = 'zzz@example.org'
        m = Member(email=test_email)
        db.session.add(m)
        db.session.commit()

        m2 = Member.get_by_email(test_email).one()
        self.assertTrue(m2 is not None)
        self.assertEquals(test_email, m2.email)


class NewsletterTest(ModelTestBaseWithFixture):

    DATASETS = (datasets.NewsletterData,)

    def test_nl_slug(self):
        # Check that 'slug' is set to something sane.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertEquals('test-nl', nl.slug)

        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl2.name).one()
        self.assertEquals('second-newsletter', nl.slug)

    def test_description_blurb(self):
        # Check that the description blurb is automatically updated.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        nl.description = u'<p>A newsletter of funny things.</p><p>Join!</p>'
        self.assertEquals(
            u'A newsletter of funny things. Join!',
            nl.description_blurb)

    def test_description_set_none(self):
        # Check that you can set description to None.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        nl.description = None
        self.assertEquals(None, nl.description)
        self.assertEquals(None, nl.description_blurb)

    def test_export(self):
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        data = nl.export()
        self.assertTrue(isinstance(data, dict))
        self.assertEquals(self.data.NewsletterData.nl1.name, data['name'])
        self.assertTrue(isinstance(data['subscriptions'], list))

    def test_import(self):
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        data = nl.export()

        # Change the NL name.
        data['name'] = u'A pretty unique newsletter name'
        nl2 = Newsletter.unexport(data)
        self.assertTrue(nl2 is not None)
        self.assertTrue(isinstance(nl2, Newsletter))

        self.assertEquals(
            nl.get_subscription_counts_by_state(),
            nl2.get_subscription_counts_by_state())


class SubscriptionsTest(ModelTestBaseWithFixture, MailTestMixin):

    def setUp(self):
        ModelTestBaseWithFixture.setUp(self)
        MailTestMixin.setUp(self)

    def tearDown(self):
        MailTestMixin.tearDown(self)
        ModelTestBaseWithFixture.tearDown(self)

    def test_nl_get(self):
        # Test fetching a Newsletter from db.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertTrue(nl is not None)
        self.assertEquals(
            self.data.NewsletterData.nl1.name, nl.name)

    def test_nl_get_subscriptions(self):
        # Count the number of subscriptions.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertEquals(3, nl.subscriptions.count())

    def test_del_member_cascade(self):
        # Removing a member should remove its subscriptions.
        m = Member.query.filter_by(
            email=self.data.MemberData.member1.email).one()
        db.session.delete(m)
        db.session.commit()
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertFalse(m in nl.subscriptions)

    def test_subscribe(self):
        # Test subscribing a mix of users that already exist,
        # and some who don't, and verify that the counts match.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        n_subs = nl.subscriptions.count()
        addrs = [self.data.MemberData.member1.email,
                 'other-user@example.com',
                 'yet-another@example.com']
        new_subs = nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        self.assertEquals(n_subs + new_subs,
                          nl.subscriptions.count())

        # Verify that opt-in emails have been sent to new users.
        self.assertEquals(
            ['other-user@example.com', 'yet-another@example.com'],
            sorted(self.sent_emails.keys()))

    def test_subscribe_with_state(self):
        # Test subscribing some users with a non-default target state.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl2.name).one()
        addrs = [self.data.MemberData.member1.email,
                 'other-user@example.com',
                 'yet-another@example.com']
        n = nl.subscribe(addrs, STATE_SUBSCRIBED)
        db.session.add(nl)
        db.session.commit()

        self.assertEquals(
            n, nl.subscriptions.filter_by(state=STATE_SUBSCRIBED).count())

    def test_subscribe_existing_member(self):
        # Test that subscribing a member creates the Subscription object.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member_not_subscribed.email]
        nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()
        
        m = Member.get_by_email(addrs[0]).one()
        self.assertEquals(1, m.subscriptions.count())

        # Opt-in email should have been sent in this case.
        self.assertEquals(addrs, self.sent_emails.keys())

    def test_subscribe_creates_members(self):
        # Test that subscribing creates Member objects.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member1.email,
                 'other-user@example.com',
                 'yet-another@example.com']
        nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        # Check that all members have been created.
        for a in addrs:
            m = Member.get_by_email(a).first()
            self.assertTrue(m is not None, 'Member %s does not exist' % a)

    def test_subscribe_member_who_opted_out(self):
        # Test that subscribe() can't modify a OPT_OUT subscription.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member_opt_out.email]
        nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        # Check that the member has *not* been subscribed.
        m = Member.query.filter_by(
            email=self.data.MemberData.member_opt_out.email).one()
        self.assertFalse(m in nl.subscriptions)

    def test_subscribe_member_already_subscribed(self):
        # Test that subscribing members twice does not create a new
        # subscription object.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        previous_subscriptions = nl.subscriptions.count()
        addrs = [self.data.MemberData.member1.email]
        nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        # Check that there was no difference.
        n_subscriptions = nl.subscriptions.count()
        self.assertEquals(previous_subscriptions, n_subscriptions)

    def test_subscribe_member_already_subscribed_does_not_override_state(self):
        # Test that subscribing members twice does not modify the existing
        # subscription object.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        previous_subscriptions = nl.subscriptions.count()

        # Pre-condition for the test.
        m = Member.get_by_email(self.data.MemberData.member1.email).one()
        self.assertEquals(STATE_OPT_IN, m.get_subscription(nl).state)

        addrs = [self.data.MemberData.member1.email]
        nl.subscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        # Check that the state was not overridden.
        self.assertEquals(STATE_OPT_IN, m.get_subscription(nl).state)

    def test_member_set_subscription_new(self):
        # Test that set_subscription() creates a new Subscription object.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        m = Member.query.filter_by(
            email=self.data.MemberData.member_not_subscribed.email).one()
        m.set_subscription(nl, STATE_SUBSCRIBED)
        db.session.add(m)
        db.session.commit()

        self.assertTrue(m.get_subscription(nl) is not None)

    def test_member_set_subscription_existing(self):
        # Test that set_subscription() can modify an existing subscription.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        m = Member.query.filter_by(
            email=self.data.MemberData.member1.email).one()
        m.set_subscription(nl, STATE_SUBSCRIBED)
        db.session.add(m)
        db.session.commit()

        self.assertTrue(m.get_subscription(nl) is not None)

    def test_mass_subscribe_and_unsubscribe(self):
        # Subscribe and unsubscribe a large number of people.
        count = 1000
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = ['user%06x@example.com' % x for x in xrange(count)]
        n = nl.subscribe(addrs)
        self.assertEquals(count, n)
        db.session.add(nl)
        db.session.commit()

        un = nl.unsubscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        self.assertEquals(un, n)

        # Verify that all opt-in emails have been sent.
        self.assertEquals(addrs, sorted(self.sent_emails.keys()))

    def test_unsubscribe(self):
        # Verify that unsubscribe() removes the Subscription object.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member1.email]
        nl.unsubscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        m = Member.get_by_email(addrs[0]).one()
        self.assertEquals(None, m.get_subscription(nl))

    def test_unsubscribe_nonmember(self):
        # Verify that unsubscribe() on a non-subscribed member is a no-op.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member_not_subscribed.email]
        nl.unsubscribe(addrs)
        db.session.add(nl)
        db.session.commit()

        m = Member.get_by_email(addrs[0]).one()
        self.assertEquals(None, m.get_subscription(nl))

    def test_unsubscribe_opt_out(self):
        # Verify that unsubscribe() does not remove an opt-out entry.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member1.email]
        nl.unsubscribe(addrs, opt_out=True)
        db.session.add(nl)
        db.session.commit()

        m = Member.get_by_email(addrs[0]).one()
        sub = m.get_subscription(nl)
        self.assertTrue(sub is not None)
        self.assertEquals(STATE_OPT_OUT, sub.state)

    def test_opt_out_unsubscribed_member(self):
        # Test that unsubscribe() with opt-out adds a Subscription object
        # if not present.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        addrs = [self.data.MemberData.member_not_subscribed.email]
        nl.unsubscribe(addrs, opt_out=True)
        db.session.add(nl)
        db.session.commit()

        m = Member.get_by_email(addrs[0]).one()
        sub = m.get_subscription(nl)
        self.assertTrue(sub is not None)
        self.assertEquals(STATE_OPT_OUT, sub.state)


class GroupsTest(ModelTestBaseWithFixture):

    def test_relationship(self):
        # Verify the relationship between Group and Newsletter.
        grp = Group.query.filter_by(
            name=self.data.GroupData.group1.name).one()
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        self.assertTrue(grp in nl.groups)

    def test_get_members(self):
        # Verify that get_members() is sane.
        grp = Group.query.filter_by(
            name=self.data.GroupData.group1.name).one()
        ms = grp.get_members()

        # None of the users should be in STATE_SUBSCRIBED.
        self.assertEquals(0, ms.count())

        # Now subscribe a user to the NL and see the change in the group.
        m = Member.query.filter_by(
            email=self.data.MemberData.member1.email).one()
        m.get_subscription(grp.newsletter).state = STATE_SUBSCRIBED
        db.session.add(m)
        db.session.flush()

        ms = grp.get_members()
        self.assertEquals(1, ms.count())

    def test_del_subscribed_member(self):
        # Test that unsubscribing from a newsletter also removes the
        # member from the group.
        grp = Group.query.filter_by(
            name=self.data.GroupData.group1.name).one()

        grp.newsletter.unsubscribe([self.data.MemberData.member1.email])
        db.session.add(grp.newsletter)
        db.session.flush()

        # The member should have disappeared from the group.
        ms = grp.get_members()
        m = Member.query.filter_by(
            email=self.data.MemberData.member1.email).one()
        self.assertFalse(m in ms)

    def test_add_subscribed_member(self):
        # Test setting the group on a new subscription.
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        nl.subscribe([self.data.MemberData.member_not_subscribed.email],
                     STATE_SUBSCRIBED)
        db.session.add(nl)
        db.session.commit()

        grp = Group.query.filter_by(
            name=self.data.GroupData.group1.name).one()
        m = Member.query.filter_by(
            email=self.data.MemberData.member_not_subscribed.email).one()
        m.get_subscription(grp.newsletter).group = grp
        db.session.add(m)
        db.session.add(grp)
        db.session.commit()

        ms = grp.get_members()
        self.assertEquals(1, ms.count())


class SignatureTest(ModelTestBaseWithFixture):

    DATASETS = (datasets.MemberData,
                datasets.NewsletterData)

    def test_sign(self):
        # Test that Member.sign() returns something.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        enc = m.sign('datum', 'salt')
        self.assertTrue(enc is not None)

    def test_verify(self):
        # Test a sign() / verify() cycle.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        enc = m.sign('datum', 'salt')
        result = m.verify(enc, 'salt')
        self.assertEquals('datum', result)

    def test_verify_bad_signature(self):
        # Verify that a bad signature causes a failure.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        self.assertRaises(
            itsdangerous.BadData,
            m.verify, 'babble babble', 'salt')

    def test_verify_bad_salt(self):
        # Verify that members have different secrets.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        m2 = Member.get_by_email(
            self.data.MemberData.member2.email).one()
        enc = m2.sign('datum', 'salt')
        self.assertRaises(
            itsdangerous.BadData,
            m.verify, enc, 'salt')

    def test_confirmation_link(self):
        # Test that the confirmation link is properly signed and
        # verifiable.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        url = m.get_confirmation_link(nl)
        url_parts = url.split('/')[-3:]
        self.assertEquals('confirm', url_parts[0])
        self.assertEquals(m.email, url_parts[1])
        self.assertEquals(nl.id,
                          m.verify(url_parts[2], 'confirm'))

    def test_unsubscribe_link(self):
        # Test that the unsubscribe link is properly signed and
        # verifiable.
        m = Member.get_by_email(
            self.data.MemberData.member1.email).one()
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        url = m.get_unsubscribe_link(nl)
        url_parts = url.split('/')[-3:]
        self.assertEquals('unsubscribe', url_parts[0])
        self.assertEquals(m.email, url_parts[1])
        self.assertEquals(nl.id,
                          m.verify(url_parts[2], 'unsubscribe'))


class MessageTest(ModelTestBaseWithFixture):

    def test_create_message(self):
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        msg = Message(newsletter=nl,
                      subject=u'Happy news!',
                      text=u'Text of the newsletter')
        db.session.add(msg)

    def test_send_message(self):
        nl = Newsletter.get_by_name(
            self.data.NewsletterData.nl1.name).one()
        msg = Message(newsletter=nl,
                      subject=u'Happy news!',
                      text=u'Text of the newsletter')
        db.session.add(msg)

        nl.send_message(msg)

        self.assertEquals(1, len(msg.deliveries))
        delivery = msg.deliveries[0]
        self.assertEquals('pending', delivery.state)


class UserTest(ModelTestBaseWithFixture):

    def test_set_password(self):
        # Sanity check for the 'password' property.
        user = User.query.filter_by(
            username=self.data.UserData.test_user.username).one()
        user.password = 'zap'
        self.assertTrue(user.password.startswith('$'))

    def test_authenticate(self):
        user = User.query.filter_by(
            username=self.data.UserData.test_user.username).one()
        user.password = 'zap'
        self.assertTrue(user.authenticate('zap'))
        self.assertFalse(user.authenticate('not-zap'))

                      
