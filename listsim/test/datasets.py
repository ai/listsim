from datetime import datetime
from fixture import DataSet


class UserData(DataSet):
    class test_user:
        username = 'test_user'
        password = 'x'
        email = 'user@example.org'
    class user2:
        username = 'user2'
        password = 'pwd2'
        email = 'another@example.org'


class MemberData(DataSet):
    class member1:
        email = '1@example.org'
    class member2:
        email = '2@example.org'
    class member3:
        email = '3@example.org'
    class member_opt_out:
        email = 'out@example.org'
    class member_not_subscribed:
        email = 'unsub@example.org'


class NewsletterData(DataSet):
    class nl1:
        name = u'test_nl'
        owner = UserData.test_user
        created_at = datetime(2012, 10, 10, 10, 10)
    class nl2:
        name = u'second newsletter!'
        owner = UserData.test_user
    class public:
        name = u'A public newsletter'
        owner = UserData.test_user
        public = True
        description = u'Some description'


class SubscriptionData(DataSet):
    class sub1:
        member = MemberData.member1
        newsletter = NewsletterData.nl1
    class sub2:
        member = MemberData.member2
        newsletter = NewsletterData.nl1
    class sub3:
        member = MemberData.member_opt_out
        newsletter = NewsletterData.nl1
        state = 'o'


class GroupData(DataSet):
    class group1:
        name = u'group1'
        newsletter = NewsletterData.nl1
        subscriptions = [
            SubscriptionData.sub1,
            SubscriptionData.sub2,
            SubscriptionData.sub3]


class MessageData(DataSet):
    class msg1:
        newsletter = NewsletterData.nl1
        subject = u'test message'
        text = u'some text'
        html = u'some html'


all_datasets = (UserData,
                MemberData,
                NewsletterData,
                SubscriptionData,
                GroupData,
                MessageData)
