import mox
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from listsim.test import *
from listsim.model import *
from listsim import app
from listsim import mailer


class MailerTestBase(mox.MoxTestBase, ModelTestBaseWithFixture):
    pass


class SMTPTest(MailerTestBase):

    def setUp(self):
        MailerTestBase.setUp(self)

        self.smtp = self.mox.CreateMock(smtplib.SMTP)
        self.mox.StubOutWithMock(smtplib, 'SMTP',
                                 use_mock_anything=True)

    def test_send_message(self):
        # Test successful delivery of a single message.
        smtplib.SMTP('localhost').AndReturn(self.smtp)
        self.smtp.sendmail(app.config['BOUNCE_ADDR'],
                           ['addr'], 'message').AndReturn({})
        self.smtp.quit()

        self.mox.ReplayAll()
        m = mailer.Mailer()
        m.send('message', 'addr')
        m.close()

    def test_send_message_fails(self):
        # Test delivery failure for unknown error.
        smtplib.SMTP('localhost').AndReturn(self.smtp)
        self.smtp.sendmail(app.config['BOUNCE_ADDR'],
                           ['addr'], 'message').AndReturn(
            {'addr': (550, 'Some local error')})

        self.mox.ReplayAll()
        m = mailer.Mailer()
        self.assertRaises(mailer.SendError, m.send, 'message', 'addr')

    def test_send_message_fails_for_unknown_recipient(self):
        # Test delivery failure for single unknown recipient.
        smtplib.SMTP('localhost').AndReturn(self.smtp)
        self.smtp.sendmail(app.config['BOUNCE_ADDR'],
                           ['addr'], 'message').AndRaise(
            smtplib.SMTPRecipientsRefused('addr'))

        self.mox.ReplayAll()
        m = mailer.Mailer()
        self.assertRaises(mailer.SendError, m.send, 'message', 'addr')

    def test_send_multiple_messages(self):
        # Verify that the connection is recycled every 10 messages.
        mailer.Mailer.CONN_RECYCLE = 10
        addrs = ['user%d' % x for x in range(20)]
        for chunk in (addrs[:10], addrs[10:]):
            smtplib.SMTP('localhost').AndReturn(self.smtp)
            for addr in chunk:
                self.smtp.sendmail(mox.IsA(str), [addr], mox.IsA(str)).AndReturn({})
            self.smtp.quit()

        self.mox.ReplayAll()

        m = mailer.Mailer()
        for addr in addrs:
            m.send('a message', addr)
        m.close()


class MessageGeneratorTest(MailerTestBase):

    DATASETS = (datasets.NewsletterData,
                datasets.MemberData)

    def setUp(self):
        MailerTestBase.setUp(self)
        self.nl = Newsletter.get_by_name(self.data.NewsletterData.nl1.name).one()
        self.member = Member.get_by_email(
            self.data.MemberData.member1.email).one()

    def test_message_multipart(self):
        # Verify that MessageGenerator correctly creates multipart messages.
        gen = mailer.MessageGenerator(
            self.nl, 'text', 'html', 'subject')
        msg = gen.msg
        self.assertTrue(isinstance(msg, MIMEMultipart))
        self.assertEquals('subject', str(msg['Subject']))
        self.assertEquals(
            app.config['BOUNCE_ADDR'], str(msg['Errors-To']))
        self.assertTrue(
            'List-Help' in msg)

    def test_message_text_only(self):
        # Test text-only message generation.
        gen = mailer.MessageGenerator(
            self.nl, 'text', None, 'subject')
        msg = gen.msg
        self.assertTrue(isinstance(msg, MIMEText))

    def test_message_html_only(self):
        # Test html-only message generation.
        gen = mailer.MessageGenerator(
            self.nl, None, 'html', 'subject')
        msg = gen.msg
        self.assertTrue(isinstance(msg, MIMEText))

    def test_message_personalization(self):
        # Test message personalization.
        gen = mailer.MessageGenerator(
            self.nl, 'text', 'html', 'subject')
        result = gen.personalize(self.member)

        self.assertTrue(('To: %s' % self.member.email) in result)

    def test_message_personalization_twice(self):
        # Check that running personalize() twice is ok (i.e. the
        # original base message is not modified).
        gen = mailer.MessageGenerator(
            self.nl, 'text', 'html', 'subject')
        for addr in [self.data.MemberData.member1.email,
                     self.data.MemberData.member2.email]:
            m = Member.get_by_email(addr).one()
            result = gen.personalize(m)
            self.assertTrue(('To: %s' % addr) in result)
            self.assertTrue('Subject: subject' in result)

