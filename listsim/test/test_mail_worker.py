import mox
import os
import tempfile
import time

from listsim.test import *
from listsim.model import *
from listsim import app
from listsim import mailer
from listsim import mail_worker


class MailerTestBase(mox.MoxTestBase, ModelTestBaseWithFixture):
    pass


class ProcessDeliveryTest(MailerTestBase):

    DATASETS = (datasets.NewsletterData,
                datasets.MemberData,
                datasets.MessageData)

    def setUp(self):
        MailerTestBase.setUp(self)

        #self.nl = Newsletter.get_by_name(self.data.NewsletterData.nl1.name).one()
        self.msg = Message.query.first()

        self.addrs = [
            self.data.MemberData.member1.email,
            self.data.MemberData.member2.email,
            self.data.MemberData.member3.email]

        self.mailer = self.mox.CreateMock(mailer.Mailer)
        self.mox.StubOutWithMock(mailer, 'Mailer', use_mock_anything=True)
        mailer.Mailer().AndReturn(self.mailer)

    def _create_delivery(self, **kw):
        d = Delivery(message=self.msg, payload={'addrs': self.addrs}, **kw)
        db.session.add(d)
        db.session.commit()
        return d

    def test_straight_run(self):
        delivery = self._create_delivery(state='pending', checkpoint=0)

        for a in self.addrs:
            self.mailer.send(mox.IsA(str), a)

        self.mox.ReplayAll()

        mail_worker.process_delivery(delivery.id)

        # Load fresh object from db.
        delivery = Delivery.query.get(delivery.id)
        self.assertEquals('done', delivery.status)
        self.assertEquals(100, delivery.progress)

    def test_partial_run(self):
        delivery = self._create_delivery(state='running', checkpoint=2)

        # Should send only 1 message.
        self.mailer.send(mox.IsA(str), self.addrs[2])

        self.mox.ReplayAll()

        mail_worker.process_delivery(delivery.id)

        # Load fresh object from db.
        delivery = Delivery.query.get(delivery.id)
        self.assertEquals('done', delivery.status)
        self.assertEquals(100, delivery.progress)


class ThreadingTest(MailerTestBase):

    def create_app(self):
        # Use a real file for multi-thread sqlite.
        self.db_fd, self.db_name = tempfile.mkstemp()
        config = dict(self.CONFIG)
        config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + self.db_name
        return create_app(config)

    def setUp(self):
        MailerTestBase.setUp(self)
        self.mox.StubOutWithMock(mail_worker, 'process_delivery_in_session')
        self.st = mail_worker.SchedulerThread(1)

    def tearDown(self):
        MailerTestBase.tearDown(self)
        os.close(self.db_fd)
        os.unlink(self.db_name)

    def test_delivery(self):
        d = Delivery(
            message=Message.query.first(),
            payload={'addrs': [
                    self.data.MemberData.member1.email]})
        db.session.add(d)
        db.session.commit()
        delivery_id = d.id
        # Expunge d from the session so we'll read it again from the
        # db after the delivery threads have run.
        db.session.expunge(d)

        mail_worker.process_delivery_in_session(delivery_id)

        self.mox.ReplayAll()

        self.st.start()
        time.sleep(0.5)
        self.st.stop()

        d = Delivery.query.get(delivery_id)
        self.assertEquals('done', d.state)
