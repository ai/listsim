import unittest
from listsim import utils


class UtilsTest(unittest.TestCase):

    def test_group_by_chunks(self):
        n = range(100)
        result = list(utils.group_by_chunks(n, 10))
        self.assertEquals(10, len(result))
        for i, chunk in enumerate(result):
            expected_chunk = range(i * 10, (i + 1) * 10)
            self.assertEquals(expected_chunk, chunk)

    def test_group_by_chunks_tiny(self):
        a = [1, 2, 3]
        result = list(utils.group_by_chunks(a))
        self.assertEquals(1, len(result))
        self.assertEquals(a, result[0])

    def test_html_to_text(self):
        h = u'<h1>header</h1><p>paragraph1</p><p>paragraph2</p>'
        expected_words = ['header', 'paragraph1', 'paragraph2']
        self.assertEquals(expected_words,
                          utils.html_to_text(h).split())

        self.assertEquals('ascii',
                          utils.html_to_text('ascii'))

    def test_styled_html_to_blurb(self):
        h = u'<h1 class="a">header</h1><p>paragraph1</p><p style="font-size:8px;" >paragraph2</p>'
        expected_words = ['header', 'paragraph1', 'paragraph2']
        self.assertEquals(expected_words,
                          utils.html_to_text(h).split())
        self.assertEquals(u'header paragraph1 paragraph2',
                          utils.text_blurb(h))

    def test_text_blurb(self):
        h = u'<h1>header</h1><p>paragraph1</p><p>paragraph2</p>'
        self.assertEquals(u'header paragraph1 paragraph2',
                          utils.text_blurb(h))
        self.assertEquals(u'header paragraph1 ...',
                          utils.text_blurb(h, 21))
