"""Views that are accessible without authentication."""

import itsdangerous
from flask import render_template, abort, request, redirect, url_for
from flask.ext.babel import gettext, lazy_gettext
from flask.ext.login import current_user
from flask.ext.wtf import Form
from wtforms import StringField
from wtforms.validators import Email, Required
from listsim import app
from listsim.model import *


class SubscribeForm(Form):
    email = StringField(
        lazy_gettext(u'Your e-mail:'),
        validators=[Required(), Email()])


@app.route('/')
def index():
    if current_user.is_authenticated():
        return redirect(url_for('list_newsletters'))
    return render_template('splash.html')


@app.route('/nls')
def list_newsletters():
    cur_page = int(request.args.get('p', 1))
    filt = (Newsletter.public == True)
    if current_user.is_authenticated():
        filt |= (Newsletter.owner == current_user)
    newsletters = Newsletter.query.filter(
        filt).order_by('name asc').paginate(cur_page, per_page=20)
    return render_template('index.html', newsletters=newsletters)


@app.route('/n/<slug>', methods=['GET', 'POST'])
def show_newsletter(slug):
    nl = Newsletter.query.filter_by(slug=slug).first_or_404()
    form = SubscribeForm()
    if form.validate_on_submit():
        nl.subscribe([form.email.data])
        db.session.add(nl)
        db.session.commit()
        return render_template('subscribe_ok.html', newsletter=nl,
                               email=form.email.data)
    return render_template('show_newsletter.html', newsletter=nl, form=form)


@app.route('/unsubscribe/<email>/<token>')
def unsubscribe(email, token):
    m = Member.get_by_email(email).first_or_404()
    try:
        nl_id = m.verify(token, 'unsubscribe')
    except itsdangerous.BadSignature, e:
        app.logger.error('Bad unsubscribe token for %s from %s: %s',
                         email, request.environ.get('REMOTE_ADDR', 'Unknown'), e)
        abort(400)
    nl = Newsletter.query.get_or_404(nl_id)
    m.set_subscription(nl, STATE_OPT_OUT)
    db.session.add(m)
    db.session.commit()
    return render_template('unsubscribe.html',
                           member=m,
                           newsletter=nl)


@app.route('/confirm/<email>/<token>')
def confirm(email, token):
    m = Member.get_by_email(email).first_or_404()
    try:
        nl_id = m.verify(token, 'confirm')
    except itsdangerous.BadSignature, e:
        app.logger.error('Bad confirmation token for %s from %s: %s',
                         email, request.environ.get('REMOTE_ADDR', 'Unknown'), e)
        abort(400)
    nl = Newsletter.query.get_or_404(nl_id)
    m.set_subscription(nl, STATE_SUBSCRIBED)
    db.session.add(m)
    db.session.commit()
    return render_template('confirm.html',
                           member=m,
                           newsletter=nl)


@app.route('/opt_out/<email>/<token>')
def global_opt_out(email, token):
    m = Member.get_by_email(email).first_or_404()
    try:
        datum = m.verify(token, 'global_opt_out')
    except itsdangerous.BadSignature, e:
        app.logger.error('Bad global_opt_out token for %s from %s: %s',
                         email, request.environ.get('REMOTE_ADDR', 'Unknown'), e)
        abort(400)
    if datum != 'global_opt_out':
        abort(400)
    m.global_opt_out = True
    db.session.add(m)
    db.session.commit()
    return render_template('global_opt_out.html',
                           member=m)
