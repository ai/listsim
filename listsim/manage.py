import json
import pprint
import random
import sys
from flask.ext.script import Manager, Command
from listsim import app, create_app
from listsim.model import *
from listsim.mail_worker import SchedulerThread


manager = Manager(create_app)


@manager.command
def initdb():
    """Initialize the database."""
    with app.app_context():
        db.create_all()
        print 'Done.'


@manager.command
def showconfig():
    """Dump configuration."""
    pprint.pprint(dict(app.config))


@manager.command
def worker(n_workers=5):
    """Run the background delivery worker."""
    sched = SchedulerThread(n_workers)
    sched.start()
    sched.join()


@manager.command
def status():
    """Print active delivery status."""
    active_deliveries = Delivery.query.filter_by(state='running').all()
    if active_deliveries:
        print '%5s %6s %4s %s' % (
            'ID', '#Recip', 'Prog', 'Newsletter')
        for d in active_deliveries:
            print '%5d %6d %-4s %s' % (
                d.id, d.num_recipients,
                '%d%%' % d.progress,
                d.message.newsletter.slug)
    else:
        print 'No active deliveries.'


def create_pass_if_none(password):
    if not password:
        password = os.urandom(6).encode('base64').strip()
        print 'Password:', password
    return password


@manager.command
def set_password(username, password=None):
    """Reset a user's password."""
    try:
        user = User.query.filter_by(username=username).one()
    except db.NoResultFound:
        print 'User not found'
        return
    user.password = create_pass_if_none(password)
    db.session.add(user)
    db.session.commit()


@manager.command
def create_nl(name, owner, password=None):
    """Create a newsletter."""
    with app.app_context():
        user = User.query.filter_by(username=owner).first()
        if not user:
            print 'Creating new user "%s"' % owner
            user = User(username=owner, email=owner,
                        password=create_pass_if_none(password))
            db.session.add(user)
        if not isinstance(name, unicode):
            name = unicode(name, 'utf-8')
        nl = Newsletter(name=name, owner=user)
        db.session.add(nl)
        db.session.commit()


@manager.command
def export_nl(name):
    """Export newsletter data to JSON."""
    with app.app_context():
        nl = Newsletter.get_by_name(name).one()
        json.dump(nl.export(), sys.stdout)


@manager.command
def import_nl():
    """Import newsletter data from JSON."""
    with app.app_context():
        data = json.load(sys.stdin)
        nl = Newsletter.unexport(data)
        db.session.add(nl)
        db.session.commit()


@manager.command
def create_test_data(domain):
    n_users = 3
    n_nl = 7
    n_members = 130

    users = []
    for i in xrange(n_users):
        user = User(username='user%d' % i,
                    email='user%d@%s' % (i, domain),
                    password='%d' % i)
        db.session.add(user)
        users.append(user)
    newsletters = []
    for i in xrange(n_nl):
        nl = Newsletter(name='nl%d' % i, owner=random.choice(users))
        db.session.add(nl)
        newsletters.append(nl)
    for i in xrange(n_members):
        m = Member(email='member%09x@%s' % (i, domain))
        db.session.add(m)
        for nl in random.sample(newsletters, random.randint(0, 2)):
            sub = Subscription(member=m, newsletter=nl,
                               state=random.choice([STATE_OPT_IN, STATE_SUBSCRIBED]))
            db.session.add(sub)
    db.session.commit()


def main():
    manager.run()


if __name__ == '__main__':
    main()
