from flask import abort, flash, request, render_template, redirect, url_for
from flask.ext.babel import gettext, lazy_gettext
from flask.ext.login import current_user, login_user, login_required, logout_user
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import Required
from listsim import app, login_manager
from listsim.model import *


class LoginForm(Form):
    username = TextField('Username', validators=[Required()])
    password = PasswordField('Password')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and user.authenticate(form.password.data):
            app.logger.info('successful login for %s', form.username.data)
            login_user(user)
            return redirect(request.args.get('next') or url_for('index'))
        app.logger.error('authentication failed for %s', form.username.data)
        flash(gettext(u'Authentication failed'), 'error')
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('logout.html')


def check_newsletter_owner(nl):
    if not current_user.is_authenticated():
        app.logger.error(
            'unauthorized access by anonymous user (owner_id=%s)', nl.owner.id)
        abort(401)
    elif current_user.id != nl.owner.id:
        app.logger.error(
            'unauthorized access by user %s (id=%s, owner_id=%s)', 
            str(current_user), current_user.get_id(), nl.owner.id)
        abort(401)
