import crypt
import itertools
import itsdangerous
import logging
import os
import re
import uuid
import time
from datetime import datetime
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from listsim import app, login_manager
from listsim import mailer
from listsim import utils

db = SQLAlchemy()
log = logging.getLogger(__name__)

# Use glibc-2.7 SHA-512 encryption.
CRYPT_ALGORITHM = 6

STATE_OPT_IN = 'i'
STATE_OPT_OUT = 'o'
STATE_SUBSCRIBED = 's'


nl_groups_table = db.Table(
    'nl_groups',
    db.Column('group_id', db.Integer, db.ForeignKey('group.id')),
    db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
    db.UniqueConstraint('group_id', 'member_id'))


def stream_match(base, attr, matches):
    return (item
            for chunk in utils.group_by_chunks(matches)
            for item in base.filter(attr.in_(chunk)))


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    newsletter_id = db.Column(db.Integer, db.ForeignKey('newsletter.id'))
    name = db.Column(db.Unicode(70), index=True)
    _unique_group = db.UniqueConstraint('name', 'newsletter_id')

    def get_members(self):
        return self.subscriptions.filter_by(state=STATE_SUBSCRIBED)


class Member(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True)
    global_opt_out = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime)
    secret = db.Column(db.BINARY(32))

    def __init__(self, **kw):
        if 'created_at' not in kw:
            kw['created_at'] = datetime.utcnow()
        db.Model.__init__(self, **kw)
        self.secret = os.urandom(32)

    def _signer(self, salt):
        return itsdangerous.URLSafeSerializer(
            self.secret, salt=salt,
            signer_kwargs={'key_derivation': 'hmac'})

    def sign(self, data, salt):
        return self._signer(salt).dumps(data)

    def verify(self, enc_data, salt):
        return self._signer(salt).loads(enc_data)

    def get_confirmation_link(self, nl):
        return '%s/confirm/%s/%s' % (
            app.config['BASE_URL'].rstrip('/'),
            self.email,
            self.sign(nl.id, 'confirm'))

    def get_unsubscribe_link(self, nl):
        return '%s/unsubscribe/%s/%s' % (
            app.config['BASE_URL'].rstrip('/'),
            self.email,
            self.sign(nl.id, 'unsubscribe'))

    def get_global_opt_out_link(self):
        return '%s/opt_out/%s/%s' % (
            app.config['BASE_URL'].rstrip('/'),
            self.email,
            self.sign('global_opt_out', 'global_opt_out'))

    def get_subscription(self, nl):
        return self.subscriptions.filter_by(newsletter=nl).first()

    def set_subscription(self, nl, state):
        # This method must be authorized by the user.
        sub = self.subscriptions.filter_by(newsletter=nl).first()
        if sub:
            sub.state = state
        else:
            self.subscriptions.append(
                Subscription(member=self, newsletter=nl, state=state))

    def send_opt_in_email(self, nl):
        m = mailer.Mailer()
        gen = mailer.SystemMessageGenerator()
        m.send(gen.personalize('opt_in', self, nl), self.email)
        m.close()

    @classmethod
    def get_by_email(cls, email):
        return cls.query.filter_by(email=email)

    @classmethod
    def from_emails(cls, addrs):
        # Read existing Members quickly, without overloading SQL.
        members = list(stream_match(cls.query, cls.email, addrs))
        missing_addrs = set(addrs) - set(x.email for x in members)
        for addr in missing_addrs:
            m = cls(email=addr)
            members.append(m)
        return members


class Subscription(db.Model):
    __tablename__ = 'subscriptions'
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'),
                          primary_key=True)
    newsletter_id = db.Column(db.Integer, db.ForeignKey('newsletter.id'),
                              primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'))
    state = db.Column(db.String(1), default=STATE_OPT_IN)
    num_bounces = db.Column(db.Integer, default=0)

    member = db.relationship(
        Member,
        backref=db.backref('subscriptions',
                           lazy='dynamic',
                           cascade='all, delete-orphan'))

    group = db.relationship(
        Group,
        backref=db.backref('subscriptions',
                           lazy='dynamic',
                           cascade='all, delete-orphan'))

    def export(self):
        return [self.member.email,
                self.state,
                self.group.name if self.group else None]

    @classmethod
    def unexport(cls, nl, data):
        email, state, group_name = data
        member = Member.get_by_email(email).first()
        if not member:
            member = Member(email=email)
        sub = cls(member=member, newsletter=nl, state=state)
        if group_name:
            group = Group.query.filter_by(newsletter=nl, name=group_name).first()
            if not group:
                group = Group(name=group_name, newsletter=nl)
            sub.group = group
        return sub


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    newsletter_id = db.Column(db.Integer, db.ForeignKey('newsletter.id'))
    subject = db.Column(db.Unicode(256))
    text = db.Column(db.UnicodeText)
    html = db.Column(db.UnicodeText)
    created_at = db.Column(db.DateTime)

    def __init__(self, **kw):
        db.Model.__init__(self, **kw)
        self.created_at = datetime.utcnow()

    def active_deliveries(self):
        return sum(True for x in self.deliveries
                   if x.state == 'running')

    def export(self):
        return {
            'subject': self.subject,
            'text': self.text,
            'html': self.html,
            'created_at': time.mktime(self.created_at.timetuple()),
            }

    @classmethod
    def unexport(cls, nl, data):
        if 'created_at' in data:
            data['created_at'] = datetime.utcfromtimestamp(data['created_at'])
        return cls(**data)


class Delivery(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    state = db.Column(db.String(10))
    payload = db.deferred(db.Column(db.PickleType))
    checkpoint = db.Column(db.PickleType)
    num_recipients = db.Column(db.Integer)
    num_errors = db.Column(db.Integer, default=0)

    # User-visible progress (0-100).
    progress = db.Column(db.Integer, default=0)

    start_at = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    completed_at = db.Column(db.DateTime)
    message_id = db.Column(db.Integer, db.ForeignKey('message.id'))

    message = db.relationship(Message,
                              backref=db.backref('deliveries'))

    def __init__(self, **kw):
        if 'start_at' not in kw:
            kw['start_at'] = datetime.utcnow()
        if 'state' not in kw:
            kw['state'] = 'pending'
        db.Model.__init__(self, **kw)
        self.created_at = datetime.utcnow()

    def update(self, checkpoint, progress=None, done=False):
        self.checkpoint = checkpoint
        self.updated_at = datetime.utcnow()
        if progress is not None:
            self.progress = progress
        if done:
            self.completed_at = datetime.utcnow()
            self.status = 'done'


class Newsletter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    _name = db.Column('name', db.Unicode(70), unique=True)
    slug = db.Column(db.String(70), unique=True)
    _description = db.Column('description', db.UnicodeText)
    description_blurb = db.Column(db.UnicodeText)
    created_at = db.Column(db.DateTime)
    public = db.Column(db.Boolean, default=False)

    subscriptions = db.relationship(
        Subscription,
        lazy='dynamic',
        cascade='all, delete-orphan',
        backref=db.backref('newsletter'))
    messages = db.relationship(
        Message,
        lazy='dynamic',
        backref=db.backref('newsletter'))
    groups = db.relationship(
        Group,
        cascade='all, delete-orphan',
        backref=db.backref('newsletter'))

    def __init__(self, **kw):
        if 'created_at' not in kw:
            kw['created_at'] = datetime.utcnow()
        db.Model.__init__(self, **kw)

    def _set_name(self, value):
        self._name = value
        self.slug = utils.slugify(value)

    def _get_name(self):
        return self._name

    name = property(_get_name, _set_name)

    def _set_description(self, value):
        self._description = value
        self.description_blurb = utils.text_blurb(value)

    def _get_description(self):
        return self._description

    description = property(_get_description, _set_description)

    def get_public_link(self):
        return '%s/nl/%s' % (
            app.config['BASE_URL'].rstrip('/'),
            self.slug)

    def get_subscription_counts_by_state(self):
        return dict(
            (tag, self.subscriptions.filter_by(state=state).count())
            for tag, state in [
                ('opt_in', STATE_OPT_IN),
                ('opt_out', STATE_OPT_OUT),
                ('subscribed', STATE_SUBSCRIBED)])

    @classmethod
    def get_by_name(cls, value):
        return cls.query.filter_by(_name=value)

    def subscribe(self, addrs, target_state=STATE_OPT_IN):
        if not isinstance(addrs, set):
            addrs = set(addrs)

        # Skip already subscribed members.
        addrs.difference_update(
            x.member.email for x in stream_match(
                self.subscriptions.join(Member), Member.email, addrs))

        new_subscriptions = 0
        for m in Member.from_emails(addrs):
            if not m.global_opt_out:
                self.subscriptions.append(
                    Subscription(member=m, state=target_state))
                new_subscriptions += 1
                if target_state == STATE_OPT_IN:
                    m.send_opt_in_email(self)
        return new_subscriptions

    def unsubscribe(self, addrs, opt_out=False):
        if not isinstance(addrs, set):
            addrs = set(addrs)

        n_unsub = 0
        seen = set()
        for sub in stream_match(
            self.subscriptions.join(Member), Member.email, addrs):
            seen.add(sub.member.email)
            if opt_out:
                sub.state = STATE_OPT_OUT
            else:
                #self.subscriptions.remove(sub)
                db.session.delete(sub)
            n_unsub += 1

        # If opting out we need to create opt-out subscriptions for
        # members that were not subscribed in the first place.
        if opt_out:
            unseen = addrs - seen
            for m in Member.from_emails(unseen):
                self.subscriptions.append(
                    Subscription(member=m, state=STATE_OPT_OUT))

        return n_unsub

    def send_message(self, msg, recipients=None, group=None, **delivery_args):
        if recipients is None:
            # The list of recipient consists of confirmed subscribers
            # (possibly filtered by 'group').
            sub_filt = {'state': STATE_SUBSCRIBED}
            if group is not None:
                sub_filt['group_id'] = group.id
            recipients = set(x.member.email for x in
                             self.subscriptions.filter_by(**sub_filt).join(Member))
        elif not isinstance(recipients, set):
            recipients = set(recipients)

        # Make absolutely sure that the global opt-out is respected.
        recipients.difference_update(
            x.member.email for x in stream_match(
                Member.query.filter_by(global_opt_out=True), Member.email, recipients))

        # Create a delivery object.
        addr_list = list(recipients)
        delivery = Delivery(message=msg,
                            checkpoint=0,
                            num_recipients=len(addr_list),
                            payload={'addrs': addr_list},
                            **delivery_args)
        db.session.add(delivery)

    def export(self):
        return {
            'owner': self.owner.email,
            'name': self.name,
            'description': self.description,
            'created_at': time.mktime(self.created_at.timetuple()),
            'public': self.public,
            'messages': [x.export() for x in self.messages],
            'subscriptions': [x.export() for x in self.subscriptions],
            }

    @classmethod
    def unexport(cls, data):
        # Auto-create all relations.
        subscriptions = data.pop('subscriptions', [])
        messages = data.pop('messages', [])

        # The owner must already exist.
        owner_email = data.pop('owner')
        owner = User.query.filter_by(email=owner_email).one()
        data['owner'] = owner

        # Convert remaining data from external format.
        if 'created_at' in data:
            data['created_at'] = datetime.utcfromtimestamp(data['created_at'])

        # Create the object and the relations.  This approach is
        # rather naive, it will be quite slow for large datasets due
        # to the vast amount of SQL queries that will be performed.
        nl = cls(**data)
        for s in subscriptions:
            nl.subscriptions.append(Subscription.unexport(nl, s))
        for m in messages:
            nl.messages.append(Messages.unexport(nl, m))
        return nl


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    _password = db.Column('password', db.String(64))
    email = db.Column(db.String(120), unique=True)
    preferred_lang = db.Column(db.String(2), default='en')
    newsletters = db.relationship(
        'Newsletter',
        backref=db.backref('owner'),
        lazy='dynamic')

    # Auto-encrypt password.
    def _set_password(self, clearpw):
        self._password = self._encrypt(clearpw)

    def _get_password(self):
        return self._password

    password = property(_get_password, _set_password)

    def _encrypt(self, pw, salt=None):
        if salt is None:
            salt = '$%d$%s$' % (CRYPT_ALGORITHM,
                                os.urandom(6).encode('base64').strip())
        return crypt.crypt(pw, salt)

    def authenticate(self, pw):
        return self.password == self._encrypt(pw, self.password)

    # The following methods are used for Flask-Login integration.
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username


@login_manager.user_loader
def load_user(userid):
    return User.query.filter_by(username=userid).first()
