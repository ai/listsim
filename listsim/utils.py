import re
import textwrap
from lxml import html
from unidecode import unidecode


def group_by_chunks(iterable, n=200):
    out = []
    for item in iterable:
        out.append(item)
        if len(out) == n:
            yield out
            out = []
    if out:
        yield out

_nonalnum_pattern = re.compile(r'[^a-z0-9]+')

def slugify(s):
    return _nonalnum_pattern.sub('-', unidecode(s).lower()).strip('-')


_paragraph_pattern = re.compile(r'(<(?:p|h[0-5]|li)(?:\s|>))', re.IGNORECASE)

def html_to_text(h):
    if not h:
        return h
    h = _paragraph_pattern.sub(r'\n\n\1', h)
    return html.fragment_fromstring(h, create_parent=True).text_content()


def wrap_text(t):
    return '\n\n'.join(textwrap.fill(x, width=75, break_long_words=False)
                       for x in t.split('\n\n'))


def text_blurb(h, target_len=512):
    if not h:
        return h
    text = html_to_text(h).split()
    out = []
    len_out = 0
    for word in text:
        n = len(word) + 1
        if (len_out + n + 3) > target_len:
            out.append('...')
            break
        len_out += n
        out.append(word)
    return ' '.join(out)

