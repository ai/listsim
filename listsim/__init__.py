import logging
import sys
from flask import Flask
from flask.ext.babel import Babel
from flask.ext.login import LoginManager
#from celery import Celery

app = Flask(__name__)
app.config['SITE_NAME'] = 'lists.im'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['BABEL_DEFAULT_LOCALE'] = 'en'
app.config['AVAILABLE_LOCALES'] = {
    'en': 'English',
    'it': 'Italiano',
    }

#app.config['CELERY_TASK_SERIALIZER'] = 'json'
#app.config['CELERY_ENABLE_UTC'] = True
#app.config['BROKER_URL'] = 'sqla+sqlite:///data.db'
#app.config['CELERY_RESULT_BACKEND'] = None
#app.config['CELERY_RESULT_SERIALIZER'] = 'json'

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'

babel = Babel(app)


@app.context_processor
def inject_global_vars():
    return dict()


def create_app(config=None):
    from listsim import auth
    from listsim import language_detection
    from listsim import user_views
    from listsim import admin_views
    from listsim.model import db

    app.config.from_envvar('APP_CONFIG', silent=True)
    if config:
        app.config.update(config)

    #if not app.config.get('BROKER_URL'):
    #    app.config.update(BROKER_URL='sqla+' + app.config['SQLALCHEMY_DATABASE_URI'])
    #celery.conf.add_defaults(app.config)

    handler = logging.StreamHandler(
        app.config.get('TESTING') and sys.stdout or sys.stderr)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    db.init_app(app)
    login_manager.init_app(app)
    return app
