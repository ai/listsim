import os
from setuptools import setup, find_packages

def _find_static_data(module, base_path):
    result = []
    abs_base = os.path.join(module, base_path)
    for root, dirs, files in os.walk(abs_base):
        for f in files:
            result.append(os.path.join(root[len(module)+1:], f))
    return result

setup(
    name="listsim",
    version="0.4",
    description="lists.im",
    author="ale",
    author_email="ale@incal.net",
    url="http://git.autistici.org/listsim",
    install_requires=["flup", "Flask", "Flask-SQLAlchemy", "Flask-Testing", "Flask-WTF",
                      "Flask-Login", "Flask-Script", "fixture", "SQLAlchemy",
                      "Flask-Babel", "itsdangerous", "lxml", "unidecode"],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    package_data={"listsim":(_find_static_data("listsim", "templates")
                             + _find_static_data("listsim", "translations")
                             + _find_static_data("listsim", "static"))},
    entry_points={
        "console_scripts": [
            "listsim-manage = listsim.manage:main",
            ],
        },
    )

